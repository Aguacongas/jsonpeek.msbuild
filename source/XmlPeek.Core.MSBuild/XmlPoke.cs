﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="JsonPoke.cs">
//   Copyright belongs to Manish Kumar
// </copyright>
// <summary>
//   Build task to replace value at Jpath
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace XmlPeek.MSBuild
{
    using System;
    using System.IO;
    using Microsoft.Build.Framework;
    using System.Xml;
    using System.Xml.XPath;

    /// <summary>
    /// Build task to convert Resource file to Java script Object Notation file
    /// </summary>
    public class XmlPoke : ITask
    {
        /// <summary>
        /// Gets or sets Build Engine
        /// </summary>
        public IBuildEngine BuildEngine { get; set; }

        /// <summary>
        /// Gets or sets Host Object
        /// </summary>
        public ITaskHost HostObject { get; set; }
      
        /// <summary>
        /// Gets or sets Xml full file path
        /// </summary>
        [Required]
        public string XmlInputPath { get; set; }

        /// <summary>
        /// Gets or sets Value
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets Query
        /// </summary>
        public string Query { get; set; }

        /// <summary>
        /// Executes the Task
        /// </summary>
        /// <returns>True if success</returns>
        public bool Execute()
        {          
            if (!File.Exists(this.XmlInputPath))
            {
                this.BuildEngine.LogMessageEvent(
                    new BuildMessageEventArgs(
                        string.Format(
                            "Skipping xml replacement, as there are no xml files found at {0}", this.XmlInputPath),
                        string.Empty,
                        "XmlPoke",
                        MessageImportance.Normal));

                return false;
            }

            if (string.IsNullOrEmpty(this.Query) || string.IsNullOrEmpty(this.Value))
            {
                this.BuildEngine.LogMessageEvent(
                    new BuildMessageEventArgs(
                        string.Format(
                            "Skipping xml replacement, no 'Query' or 'Value'"),
                        string.Empty,
                        "XmlPoke",
                        MessageImportance.Normal));

                return false;
            }

            this.BuildEngine.LogMessageEvent(
                new BuildMessageEventArgs(
                    string.Format("Started xml poke for file {0}", this.XmlInputPath),
                    string.Empty,
                    "XmlPoke",
                    MessageImportance.Normal));

            try
            {
                // Replacing the value 
                var document = new XPathDocument(File.OpenRead(this.XmlInputPath));
                
                var navigator = document.CreateNavigator();
                var node = navigator.SelectSingleNode(this.Query);

                this.BuildEngine.LogMessageEvent(
                new BuildMessageEventArgs(
                    string.Format("Replace value {0} by {1}", node.Value, this.Value),
                    string.Empty,
                    "XmlPoke",
                    MessageImportance.Normal));
                
                File.WriteAllText(this.XmlInputPath, navigator.OuterXml);
            }
            catch (Exception)
            {
                // Adding information about jpath and full filepath for debugging purposes 
                this.BuildEngine.LogErrorEvent(
                    new BuildErrorEventArgs(
                        string.Empty,
                        string.Empty,
                        this.XmlInputPath,
                        0,
                        0,
                        0,
                        0,
                        string.Format("Failed to replace Querry:{0} in file:{1}", this.Query, this.XmlInputPath),
                        string.Empty,
                        "XmlPoke"));
                throw;
            }

            return true;
        }
    }
}