using Microsoft.Build.Framework;
using Moq;
using System;
using System.IO;
using Xunit;

namespace JsonPeek.Core.MSBuild.Test
{
    public class JsonPeekTest
    {
        [Fact]
        public void Execute_with_jscontent_should_not_fail()
        {
            var sut = new JsonPeek.MSBuild.JsonPeek();

            sut.JsonContent = File.ReadAllText(AppContext.BaseDirectory + @"\version.json");
            sut.JPath = "Major";
            sut.BuildEngine = new Mock<IBuildEngine>().Object;
            sut.Execute();
        }

        [Fact]
        public void Execute_with_jpath_should_not_fail()
        {
            var sut = new JsonPeek.MSBuild.JsonPeek();

            sut.JPath = AppContext.BaseDirectory + @"\version.json";
            sut.JPath = "Major";
            sut.BuildEngine = new Mock<IBuildEngine>().Object;
            sut.Execute();
        }
    }
}
