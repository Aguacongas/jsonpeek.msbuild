﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Build.Framework;
using XmlPeek.MSBuild;
using Xunit;
using Moq;

namespace XmlPeek.Core.MSBuild.Test
{
    public class XmlPokeTest
    {
        [Fact]
        public void ExcecuteTest()
        {
            var sut = new XmlPoke();
            sut.XmlInputPath = @"C:\Projects\jsonpeek.msbuild\source\XmlPeek.Core.MSBuild.Test\version.props";
            sut.Query = "/Project/PropertyGroup/BuildNumber";
            sut.Value = "test";
            sut.BuildEngine = new Mock<IBuildEngine>().Object;
            sut.Execute();
        }
    }
}
